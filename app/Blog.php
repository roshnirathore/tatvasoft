<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'title','decription', 'start_date', 'end_date','is_Active','image','user_id'
    ];
}
