<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Auth;
use App\Blog;
use App\User;
use Redirect;
class BlogController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user_data = Auth::user();
        if($user_data->role == 'admin'){
           $blogs =  Blog::where('is_Active','1')->whereDate('end_date' ,'>=' , date('Y-m-d'))->paginate(10);
        }else{
            $blogs =  Blog::where('user_id',$user_data->id)->whereDate('end_date' ,'>=' , date('Y-m-d'))->where('is_Active','1')->paginate(10);
        }
        return view('home', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'title'       => ['required', 'string'],
            'description' => ['required'],
            'start_date'  => ['required'],
            'end_date'    => ['required'],
            'status'      => ['required'],
            'image'       => ['required', 'image','mimes:jpg,png,jpeg','max:2048'],
        ]);

        $user_img = $request->file('image');
        $img_name = time()."_user_blog.".$user_img->getClientOriginalExtension();
        $path = 'blogImages/';
        $url = $path.$img_name;
        $upload = $user_img->move($path , $img_name);
     
          Blog::create([
            'title'       => $request->input('title'),
            'decription'  => $request->input('description'),
            'start_date'  => $request->input('start_date'),
            'end_date'    => $request->input('end_date'),
            'is_Active'   => $request->input('status'),
            'image'       => $img_name,
            'user_id'     => Auth::user()->id,
        ]);

        return redirect::route('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Blog::find($id);
        if($data){ 
            return view('blog.edit', compact('data'));
        }
        return Redirect::back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'title'       => ['required', 'string'],
            'description' => ['required'],
            'start_date'  => ['required'],
            'end_date'    => ['required'],
            'status'      => ['required'],
            'image'       => ['required', 'image','mimes:jpg,png,jpeg','max:2048'],
        ]);
        $blog_id = $request->input('id');
        $blog = Blog::find($blog_id);
        if($request->file('image')){
            $user_img = $request->file('image');
            $img_name = time()."_user_blog.".$user_img->getClientOriginalExtension();
            $path = 'blogImages/';
            $url = $path.$img_name;
            $upload = $user_img->move($path , $img_name);
        }else{
            $img_name = $blog['image'];
        }
            $blog->title       = $request->input('title');
            $blog->decription  = $request->input('description');
            $blog->start_date  = $request->input('start_date');
            $blog->end_date    = $request->input('end_date');
            $blog->is_Active   = $request->input('status');
            $blog->image       = $img_name;
        
            $blog->save();
        return redirect::route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Blog::find($id);
        if($res){
            $res->delete();
            return Redirect::back();
        }
        return Redirect::back();
    }
}
