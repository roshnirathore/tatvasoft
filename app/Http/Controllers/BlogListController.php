<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BlogListController extends Controller
{   
    //Display All blogs without login
    public function index()
    {   
        $blogs =  Blog::where('is_Active','1')->whereDate('end_date' ,'>=' , date('Y-m-d'))->get();
        return view('blog.list', compact('blogs'));
    }
}
