<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\User;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        $user_data = Auth::user();
        if($user_data->role == 'admin'){
           $blogs =  Blog::where('is_Active','1')->whereDate('end_date' ,'>=' , date('Y-m-d'))->paginate(10);
        }else{
            $blogs =  Blog::where('user_id',$user_data->id)->whereDate('end_date' ,'>=' , date('Y-m-d'))->where('is_Active','1')->paginate(10);
        }
        return view('home', compact('blogs'));
    }
}
