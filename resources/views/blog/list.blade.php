@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('All Blogs') }}
                </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(!empty($blogs))
                    <table border="5">
                        <thead>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Image</th>
                        </thead>
                        <tbody>
                        @foreach($blogs as $blog)
                        <tr>
                            <td>{{ $blog->title }}</td>
                            <td>{{ substr($blog->decription,0, 10) }}</td>
                            <td>{{ $blog->start_date }}</td>
                            <td>{{ $blog->end_date }}</td>
                            <td><img style="height: 30px;" src ='{{url("/blogImages")}}/{{ $blog->image }}'/></td>
                            
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @else
                    <div>No Blog available <div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
