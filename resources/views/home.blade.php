@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Blogs') }}
                <div><a href="{{ url('/create_blog') }}">Add</a> </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(!empty($blogs))
                    <table border="5">
                        <thead>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Image</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </thead>
                        <tbody>
                        @foreach($blogs as $blog)
                        <tr>
                            <td>{{ $blog->title }}</td>
                            <td>{{ substr($blog->decription,0, 20) }}</td>
                            <td>{{ $blog->start_date }}</td>
                            <td>{{ $blog->end_date }}</td>
                            <td><img style="height: 30px;" src ='{{url("/blogImages")}}/{{ $blog->image }}'/></td>
                            <td>
                                <a href="blog_edit/{{ $blog->id }}" >Edit</a>
                            </td>
                            <td>
                                <a href="blog_delete/{{ $blog->id }}" >Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        
                    </table>
                    {{ $blogs->links() }}
                    @else
                    <div>No Blog available <div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
