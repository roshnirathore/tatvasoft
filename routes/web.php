<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlogListController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('create_blog', 'BlogController@create')->name('create_blog');
Route::post('save_blog', 'BlogController@store')->name('save_blog');
Route::get('blog_edit/{id}', 'BlogController@edit')->name('blog_edit');
Route::post('update_blog', 'BlogController@update')->name('update_blog');
Route::get('blog_delete/{id}', 'BlogController@destroy')->name('blog_delete');
